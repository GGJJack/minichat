package io.gitlab.ggjjack.minichat.model;

import com.google.firebase.database.Exclude;

public class Message {
    private static String myToken = null;

    public static void initToken(String token) {
        myToken = token;
    }

    public static Message newMessage(String text) {
        if (myToken == null) {
            throw new IllegalAccessError("토큰 초기화를 먼저 진행하세요");
        }
        return new Message(myToken, text);
    }

    @Exclude
    public String key;
    public String writer;
    public String text;

    public Message() {
    }

    public Message(String writer, String text) {
        this.writer = writer;
        this.text = text;
    }

    @Exclude
    public boolean isMyMessage() {
        return this.writer != null && this.writer.equals(myToken);
    }
}
