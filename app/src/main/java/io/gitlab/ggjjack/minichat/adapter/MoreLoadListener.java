package io.gitlab.ggjjack.minichat.adapter;

import android.support.annotation.NonNull;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;

public abstract class MoreLoadListener extends RecyclerView.OnScrollListener {
    private int mMoreLoadCount = 2;
    private boolean mIsLoading = false;
    private LinearLayoutManager mLayoutManager;

    public MoreLoadListener(LinearLayoutManager layoutManager) {
        mLayoutManager = layoutManager;
    }

    @Override
    public void onScrolled(@NonNull RecyclerView recyclerView, int dx, int dy) {
        super.onScrolled(recyclerView, dx, dy);
        if (!mIsLoading) {
            if (mLayoutManager.findFirstVisibleItemPosition() < mMoreLoadCount) {
                mIsLoading = true;
                onMoreLoad();
            }
        }
    }

    public void setMoreLoadCount(int count) {
        mMoreLoadCount = count;
    }

    public void loadFinish() {
        mIsLoading = false;
    }

    public abstract void onMoreLoad();
}
