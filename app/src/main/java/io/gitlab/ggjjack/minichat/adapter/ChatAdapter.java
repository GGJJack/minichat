package io.gitlab.ggjjack.minichat.adapter;

import android.support.annotation.NonNull;
import android.support.constraint.ConstraintLayout;
import android.support.constraint.ConstraintSet;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.google.firebase.database.FirebaseDatabase;

import java.util.ArrayList;
import java.util.Collection;

import butterknife.BindView;
import butterknife.ButterKnife;
import io.gitlab.ggjjack.minichat.R;
import io.gitlab.ggjjack.minichat.model.Message;

public class ChatAdapter extends RecyclerView.Adapter<ChatAdapter.MessageViewHolder> {

    private ArrayList<Message> mChatMessages = new ArrayList<>();

    public ChatAdapter() {

    }

    @NonNull
    @Override
    public MessageViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        return new MessageViewHolder(LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.item_message, viewGroup, false));
    }

    @Override
    public void onBindViewHolder(@NonNull MessageViewHolder viewHolder, int i) {
        viewHolder.bind(mChatMessages.get(i));
    }

    @Override
    public int getItemCount() {
        return mChatMessages.size();
    }

    public String getFirstKey() {
        if (mChatMessages.size() <= 0) return null;
        return mChatMessages.get(0).key;
    }

    public void addMessageAt(int index, Collection<Message> message) {
        mChatMessages.addAll(index, message);
        notifyItemRangeInserted(index, message.size());
    }

    public void addMessage(Message message) {
        mChatMessages.add(message);
        notifyItemInserted(mChatMessages.size());
    }

    class MessageViewHolder extends RecyclerView.ViewHolder {
        @BindView(R.id.tvMessage)
        TextView tvMessage;
        @BindView(R.id.tvWriter)
        TextView tvWriter;

        MessageViewHolder(@NonNull View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }

        void bind(Message message) {
            ConstraintSet constraintSet = new ConstraintSet();
            constraintSet.clone((ConstraintLayout) itemView);
            if (message.isMyMessage()) {
                tvWriter.setVisibility(View.GONE);
                constraintSet.connect(R.id.tvMessage, ConstraintSet.RIGHT, ConstraintSet.PARENT_ID, ConstraintSet.RIGHT);
                constraintSet.clear(R.id.tvMessage, ConstraintSet.LEFT);
            } else {
                tvWriter.setVisibility(View.VISIBLE);
                tvWriter.setText(message.writer);
                constraintSet.connect(R.id.tvMessage, ConstraintSet.LEFT, ConstraintSet.PARENT_ID, ConstraintSet.LEFT);
                constraintSet.clear(R.id.tvMessage, ConstraintSet.RIGHT);
            }
            tvMessage.setText(message.text);
            constraintSet.applyTo((ConstraintLayout) itemView);
        }
    }
}
