package io.gitlab.ggjjack.minichat;

import android.content.DialogInterface;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.View;
import android.widget.EditText;

import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.AuthResult;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.ChildEventListener;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import io.gitlab.ggjjack.minichat.adapter.ChatAdapter;
import io.gitlab.ggjjack.minichat.adapter.MoreLoadListener;
import io.gitlab.ggjjack.minichat.common.Constants;
import io.gitlab.ggjjack.minichat.model.Message;

public class MainActivity extends AppCompatActivity {
    @BindView(R.id.rvChat)
    RecyclerView rvChat;
    @BindView(R.id.edtMessage)
    EditText edtMessage;

    private FirebaseAuth mAuth;
    private FirebaseUser mUserInfo;

    private DatabaseReference mFbChat;

    private ChatAdapter mChatAdapter;
    private LinearLayoutManager mLayoutManager;
    private MoreLoadListener mMoreLoadListener;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        ButterKnife.bind(this);
        mAuth = FirebaseAuth.getInstance();
        FirebaseDatabase database = FirebaseDatabase.getInstance();
        mFbChat = database.getReference("message");
        initChat();
    }

    private void initChat() {
        mChatAdapter = new ChatAdapter();
        mLayoutManager = new LinearLayoutManager(this, LinearLayoutManager.VERTICAL, false);
        mMoreLoadListener = new MoreLoadListener(mLayoutManager) {
            @Override
            public void onMoreLoad() {
                Log.i("HJ", "Try More Load");
                loadChatMessage();
            }
        };
        rvChat.setAdapter(mChatAdapter);
        rvChat.setLayoutManager(mLayoutManager);
        rvChat.addOnLayoutChangeListener(mFocusKeyboard);
        rvChat.addOnScrollListener(mMoreLoadListener);
    }

    private void loadChatMessage() {
        mFbChat.orderByKey()
                .endAt(mChatAdapter.getFirstKey())
                .limitToLast(Constants.MORE_LOAD)
                .addValueEventListener(mFirebaseLoadListener);
    }

    private ValueEventListener mFirebaseLoadListener = new ValueEventListener() {
        @Override
        public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
            ArrayList<Message> messages = new ArrayList<>();
            for (DataSnapshot snapshot : dataSnapshot.getChildren()) {
                Message message = snapshot.getValue(Message.class);
                if (message != null && !snapshot.getKey().equals(mChatAdapter.getFirstKey())) {
                    message.key = snapshot.getKey();
                    messages.add(message);
                }
            }
            if (messages.size() > 0) {
                mChatAdapter.addMessageAt(0, messages);
                mMoreLoadListener.loadFinish();
            }
        }

        @Override
        public void onCancelled(@NonNull DatabaseError databaseError) {
            Log.e("HJ", "Error : " + databaseError.getMessage());
        }
    };

    private View.OnLayoutChangeListener mFocusKeyboard = new View.OnLayoutChangeListener() {
        @Override
        public void onLayoutChange(View view, int l, int t, int r, int b, int ol, int ot, int or, int ob) {
            if (b < ob) {
                rvChat.postDelayed(new Runnable() {
                    @Override
                    public void run() {
                        rvChat.smoothScrollToPosition(mChatAdapter.getItemCount() - 1);
                    }
                }, 100);
            }
        }
    };

    @Override
    protected void onStart() {
        super.onStart();
        mUserInfo = mAuth.getCurrentUser();
        if (mUserInfo != null) {
            startLoadChat();
        } else {
            mAuth.signInAnonymously().addOnCompleteListener(this, new OnCompleteListener<AuthResult>() {
                @Override
                public void onComplete(@NonNull Task<AuthResult> task) {
                    if (task.isSuccessful()) {
                        Log.i("HJ", "Login Success");
                        mUserInfo = mAuth.getCurrentUser();
                        startLoadChat();
                    } else {
                        Log.i("HJ", "Login Failed");
                        errorFinish();
                    }
                }
            });
        }
    }

    private void errorFinish() {
        new AlertDialog.Builder(this)
                .setTitle(R.string.svr_err_title)
                .setMessage(R.string.svr_err_message)
                .setPositiveButton(android.R.string.ok, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {
                        finish();
                    }
                })
                .setOnCancelListener(new DialogInterface.OnCancelListener() {
                    @Override
                    public void onCancel(DialogInterface dialogInterface) {
                        finish();
                    }
                })
                .create().show();
    }

    @OnClick(R.id.btnSend)
    public void onSend() {
        String message = edtMessage.getText().toString();
        if (message.isEmpty()) return;
        mFbChat.push().setValue(Message.newMessage(message));
        edtMessage.setText("");
    }

    private void startLoadChat() {
        if (mUserInfo == null) return;
        Message.initToken(mUserInfo.getUid());
        mFbChat.orderByKey().limitToLast(Constants.FIRST_LOAD).addChildEventListener(mChatMessageEvent);
    }

    private void addMessage(Message message) {
        Log.i("HJ", "Message : " + message.text);
        mChatAdapter.addMessage(message);
        rvChat.scrollToPosition(mChatAdapter.getItemCount() - 1);
    }

    private ChildEventListener mChatMessageEvent = new ChildEventListener() {
        @Override
        public void onChildAdded(@NonNull DataSnapshot dataSnapshot, @Nullable String s) {
            Message value = dataSnapshot.getValue(Message.class);
            if (value == null || value.text == null || value.text.isEmpty()) return;
            value.key = dataSnapshot.getKey();
            addMessage(value);
        }

        @Override
        public void onChildChanged(@NonNull DataSnapshot dataSnapshot, @Nullable String s) {

        }

        @Override
        public void onChildRemoved(@NonNull DataSnapshot dataSnapshot) {

        }

        @Override
        public void onChildMoved(@NonNull DataSnapshot dataSnapshot, @Nullable String s) {

        }

        @Override
        public void onCancelled(@NonNull DatabaseError databaseError) {

        }
    };
}
